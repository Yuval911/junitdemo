import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class ExampleTest {
    @Test
    public void returnsOneIfGivenOne() {
        Example example = new Example();
        String result = example.getResult(1);
        assertEquals("1", result);
    }
}
